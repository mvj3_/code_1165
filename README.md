滑动 UIScrollView 时，UIPageControl 出现在状态栏（UIStatusBar）上，并且遮挡住状态栏。UIScrollView 滚动结束时，UIPageControl 消失，状态栏重新出现。这种滚动视图的效果在Groupon 的app中使用过。	
